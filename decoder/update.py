"""

The following functions have to be called with decoder.py imported as
"import decoder", not "from decoder import *".

CheckForUpdates() --> dictionary or None
      Checks for updates of decoder.py on the official page and mirror.
      If you are not connected to the internet or both servers are unavailable or the downloaded
      update script is corrupted, None is returned. If everything went fine, the dictionary of
      update script options, with their values, is returned.
      To check for new version do:
      >>> nv = decoder.CheckForUpdates()
      >>> if nv:
      >>>   if nv["__version__"] <= decoder.__version__:
      >>>     print "No new version available!"
      >>>   else:
      >>>     print "A new version is", nv["__version__"]
      >>> else:
      >>>   print "Something wrong with net connection, DNS, servers, uscripts etc."

update (udict=0) --> integer
      Updates decoder.py. This function is designed to help programers when creating
      software like players etc. Instead of packing new decoder.py with updates of your program,
      make it update itself.  This way, you can be sure that the users have the latest stable
      official version.
      udict argument is a dictionary returned by CheckForUpdates() function.
      If it is 0 (default) the CheckForUpdates() will be called automatically.
      Firstly, the update() function, tries to make a backup of important files, before it starts
      updating them.  If something went wrong while backing up, the error will not be raised.
      After making backup (ZIP file named decoder-__version__-backup.zip) update() tries to download
      ZIP file and unpack it.  The existing files will be replaced with newones.
      This function may, also, add some new files and so be careful when you are using it.
      Keep decoder.py isolated from other parts of your programs and everything will be fine.
      The updated version will be active when you start your program again.
      If you/your program discover that new version makes problem, you can easily rollback to the
      old one using restore() function. You should try the new version right away, and rollback if
      something is not working, because you cannot be sure if you would be able to access the
      restore() function after importing a new version.
      However, it is unlikely that you would ever need to use restore() function.
      'decoder.py' is strictly backward compatible and heavily tested for bugs.
      update() returns 0 if it fails in any sense, -1, if there is no new version available, and 1
      if everything went fine.  This function requires write permissions on current working
      directory of decoder.py.
      WARNING:
            It may happen, that someone redirects your DNS to fool your system and tricks it to
            download the files from another location, pretending to be brailleweb.com. The
            downloaded files may contain malicious code and harm your system.
            I hope that this warning would not give somebody an idea.

restore (v=None) --> None
      Rolls back the decoder.py from the backup file.
      If you ran the new version and want to restore it to the previous one,
      you need to specify the version of previous version.
      If you test the decoder.py and you want to restore to previous one, the restore() will be
      enough.
      The function tries to achieve its goal, but does not raise an error if there is one.
      The adequate backup file 'decoder-__version__-backup.zip' needs to be present in the directory
      i.e. the update() must be called first.
"""

def CheckForUpdates ():
    """Checks for updates of decoder.py on the official page and mirror.
    If you are not connected to the internet or both servers are unavailable or the downloaded update script is corrupted,
    None is returned. If everything went fine, the dictionary of update script options, with their values, is returned.
    To check for new version do:
    >>> nv = decoder.CheckForUpdates()
    >>> if nv:
    >>>   if nv["__version__"] <= decoder.__version__:
    >>>     print "No new version available!"
    >>>   else:
    >>>     print "A new version is", nv["__version__"]
    >>> else:
    >>>   print "Something wrong with net connection, DNS, servers, uscripts etc."
    """
    from urllib2 import urlopen, Request
    from sys import platform
    from ConfigParser import ConfigParser
    # The first URL is a script that counts update checkers
    # It increases the counter and returns the file stored on second URL
    # Nothing to worry about, anyway. Just statistics.
    urls = ("http://www.brailleweb.com/cgi-bin/uscript.py",
            "http://www.brailleweb.com/projects/decoder.py",
            "http://brailleweb.webhop.net/decoder.py/updates.pdc")
    for x in urls:
        try:
            req = Request(x)
            req.add_header("User-Agent", "decoder.py/"+__version__+" on "+platform)
            u = urlopen(req)
            cp = ConfigParser()
            cp.readfp(u)
            d = {}
            for y in cp.options("decoder.py"): d[y] = cp.get("decoder.py", y)
            return d
        except: pass

def update (udict=0):
    """Updates decoder.py. udict argument is a dictionary returned by CheckForUpdates() function.
    If it is 0 (default) the CheckForUpdates() will be called automaticaly.
    Firstly, the update() function, tries to make a backup of important files, before it starts updating them.
    If something went wrong while backing up, the error will not be raised.
    After making backup (ZIP file named decoder-__version__-backup.zip) update() tries to download ZIP file and unpack it.
    The existing files will be replaced with newones.
    This function may, also, add some new files and so be carefull when you are using it.
    Keep decoder.py isolated from other parts of your programs and everything will be fine.
    The updated version will be active when you start your program again.
    If you/your program discover that new version makes problem, you can easily rollback to the old one using
    restore() function. You should try the new version right away, and rollback if something is not working, because
    you cannot be sure if you would be able to access the restore() function after importing a new version.
    However, it is unlikely that you would ever need to use restore() function.
    update() returns 0 if it fails in any sense, -1, if there is no new version available, and 1 if everything went fine.
    This function requires write permissions on current working directory.
    """
    if udict is 0: d = CheckForUpdates()
    else: d = udict
    if not d: return 0
    if d["__version__"] <= __version__: return -1
    # But may be that the new version is for Unix only and you are running Windows
    if os.name=="nt" and d["new_win32"]=="no": return -1
    if os.name!="nt" and d["new_unix"]=="no": return -1
    # Change the directory to the directory where this file is living
    olddir = os.getcwd()
    try: os.chdir(os.path.dirname(os.path.abspath(__file__)))
    except: pass
    from urllib2 import urlopen, Request
    from zipfile import ZipFile
    from sys import platform
    # Try to make a backup
    ld = [x.strip() for x in d[__version__.lower()+"-"+("unix", "win32")[os.name=="nt"]+"_filelist"].split(";")]
    # ld is a list of decoder.py important files (of current version)
    # If you created a file which current version does not contain, and new one does,
    # your file will be backed up as well, and replaced by decoder.py one (unfortunately)
    # But the 'will be backed up' part is important
    try: bz = ZipFile("decoder-"+__version__+"-backup.zip", "w")
    except: os.chdir(olddir); return 0
    # Because, if backup fails, then, every other writting action will fail as well.
    for x in ld:
        try: bz.write(x.replace("/", os.sep), x)
        except: pass
    # Fetch new version
    from cStringIO import StringIO
    from __builtin__ import open
    fn = "decoder-"+d["__version__"]+"-" + ("Unix", "Win32")[os.name=="nt"] + ".zip"
    for x in (d["updateurl"], d["mirror"]):
        try:
            req = Request(x+fn)
            req.add_header("User-Agent", "decoder.py/"+__version__+" on "+platform)
            u = urlopen(req)
            z = ZipFile(StringIO(u.read()), "r")
            nl = z.namelist()
            for y in nl:
                # More backing ups
                if os.path.exists(y.replace("/", os.sep)) and y not in ld:
                    try: bz.write(y.replace("/", os.sep), y)
                    except: pass
                yp = y.split("/")
                if len(yp)>1:
                    ydir = os.sep.join(yp[:-1])
                    if not os.path.exists(ydir): os.makedirs(ydir)
                if not yp[0] or not yp[-1]: continue
                f = open(y.replace("/", os.sep), "wb")
                f.write(z.read(y))
                f.close()
            z.close(); bz.close()
            os.chdir(olddir)
            return 1
        except Exception as e: print(e)
    bz.close()
    os.chdir(olddir)
    return 0

def restore (v=None):
    """Rolls back the decoder.py from the backup file.
    If you ran the new version and want to restore it to the previous one,
    you need to specify the version of previous version.
    If you test the decoder.py and you want to restore to previous one, the restore() will be enough.
    The function tries to achieve its goal, but does not raise an error if there is one."""
    if not v: v = __version__
    olddir = os.getcwd()
    try: os.chdir(os.path.dirname(os.path.abspath(__file__)))
    except: pass
    fn = "decoder-"+v+"-backup.zip"
    if not os.path.exists(fn): os.chdir(olddir); return
    from zipfile import ZipFile
    from __builtin__ import open
    # Just, if read permissions are not here
    try: z = ZipFile(fn, "r")
    except: os.chdir(olddir); return
    for x in z.namelist():
        try:
            f = open(x.replace("/", os.sep), "wb")
            f.write(z.read(x))
            f.close()
        except: pass
    z.close(); os.chdir(olddir)
